﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Calculadora : Form
    {
        double primero, segundo;
        string operador;

        public Calculadora()
        {
            InitializeComponent();
        }
        Clases.clsSuma obj = new Clases.clsSuma();
        Clases.clsResta obj2 = new Clases.clsResta();
        Clases.clsMultiplicacion obj3 = new Clases.clsMultiplicacion();
        Clases.clsDivision obj4 = new Clases.clsDivision();
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnN0_Click(object sender, EventArgs e)
        {
            tbxScreen.Text= tbxScreen.Text+"0";
        }

        private void btnN1_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "1";
        }

        private void btnN2_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "2";
        }

        private void btnN3_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "3";
        }

        private void btnN4_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "4";
        }

        private void btnN5_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "5";
        }

        private void btnN6_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "6";
        }

        private void btnN7_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "7";
        }

        private void btnN8_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "8";
        }

        private void btnN9_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + "9";
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            operador = "+";
            primero = double.Parse(tbxScreen.Text);
            tbxScreen.Clear();
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            operador = "-";
            primero = double.Parse(tbxScreen.Text);
            tbxScreen.Clear();
        }

        private void btnMultiplicacion_Click(object sender, EventArgs e)
        {
            operador = "*";
            primero = double.Parse(tbxScreen.Text);
            tbxScreen.Clear();
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            operador = "/";
            primero = double.Parse(tbxScreen.Text);
            tbxScreen.Clear();
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            segundo= double.Parse(tbxScreen.Text);
            double sum;
            double res;
            double mul;
            double div;

            switch (operador)
            {
                case "+":
                    sum = obj.Sumar((primero), (segundo));
                    tbxScreen.Text = sum.ToString();
                    break;
                case "-":
                    sum = obj2.Restar((primero), (segundo));
                    tbxScreen.Text = sum.ToString();
                    break;
                case "*":
                    sum = obj3.Multiplicar((primero), (segundo));
                    tbxScreen.Text = sum.ToString();
                    break;
                case "/":
                    sum = obj4.Division((primero), (segundo));
                    tbxScreen.Text = sum.ToString();
                    break;
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (tbxScreen.Text.Length == 1)
                tbxScreen.Text = "";
           // else
               // tbxScreen.Text = tbxScreen.Text.Substring(0, tbxScreen.Text.Length - 1);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
             tbxScreen.Clear();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tbxScreen.Text = tbxScreen.Text + ".";
        }
    }
}
